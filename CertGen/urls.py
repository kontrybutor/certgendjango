"""CertGen URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views
from app import views as app_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', app_views.generate, name='home'),
    path('login/', auth_views.LoginView.as_view(template_name='app/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='app/logout.html'), name='logout'),
    path('signup/', app_views.signup, name='signup'),
    path('account_activation_sent/', app_views.account_activation_sent, name='account_activation_sent'),
    path('activate/<uidb64>[0-9A-Za-z_\-]/<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20}/',
         app_views.ActivateAccountView.as_view(), name='activate'),
    path('certificates_list/', app_views.get_certificates_for_user, name='certificates'),
    path('admin_view/', app_views.get_certificates_for_admin, name='admin_view'),
    path('contact/', app_views.contact, name='contact'),
    path('terms/', app_views.terms_and_conditions, name='terms'),
    path('policy/', app_views.policy, name='policy'),
    path('press/', app_views.press, name='press'),
    path('about', app_views.about, name='about'),
    path('pricing', app_views.pricing, name='pricing'),

]
