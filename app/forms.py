from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import SelectDateWidget

from .models import Certificate


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)


class GenerateForm(forms.ModelForm):
    valid_from = forms.DateField(
        widget=SelectDateWidget(
        ))
    valid_to = forms.DateField(
        widget=SelectDateWidget())

    class Meta:
        model = Certificate

        fields = ('name', 'country', 'city', 'valid_from', 'valid_to', 'key', 'signature')
