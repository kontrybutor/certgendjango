import random
import string
import requests
import json
import datetime
import logging
import time
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views import View

from .filters import CertificateFilter
from .models import Certificate
from .forms import SignUpForm, GenerateForm
from .tokens import account_activation_token


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()

            current_site = get_current_site(request)
            subject = 'Activate Your CertGen Account'
            message = render_to_string('account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message, from_email='postmaster@certgen.com.pl')

            return redirect('account_activation_sent')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def account_activation_sent(request):
    return render(request, 'account_activation_sent.html')


def press(request):
    return render(request, 'press.html')


def about(request):
    return render(request, 'about.html')


def pricing(request):
    return render(request, 'pricing.html')


@login_required
def generate(request):
    key = ""
    if request.method == 'POST':
        form = GenerateForm(request.POST)
        if form.is_valid():

            subject_name = 'C=' + form.cleaned_data['country'] + ',' + 'L=' + form.cleaned_data[
                'city'] + ',' + 'O=' + str(request.user) + ',' + 'CN=' \
                           + form.cleaned_data['name']
            valid_from = form.cleaned_data['valid_from'].strftime('%Y-%m-%d')
            valid_to = form.cleaned_data['valid_to'].strftime('%Y-%m-%d')
            key_len = form.cleaned_data['key']
            sig_alg = form.cleaned_data['signature']
            url = 'http://localhost:9080/gen/1/' + subject_name + '/' + valid_from + '/' + valid_to + '/' + key_len + '/' + sig_alg
            r = requests.post(url)
            if r.status_code == 201:
                key = r.text
                certificate = form.save(commit=False)
                certificate.user = request.user
                certificate.generated_key = key
                certificate.save()
            else:
                key = "ERROR DURING GENERATION"
    else:
        form = GenerateForm()

    return render(request, 'home.html', {'form': form, 'key': key})


@login_required
def get_certificates_for_user(request):
    logged_user = request.user
    certificates_list = Certificate.objects.filter(user=logged_user).order_by('name')
    certificate_filter = CertificateFilter(request.GET, queryset=certificates_list)
    certificates_list = certificate_filter.qs
    page = request.GET.get('page', 1)
    paginator = Paginator(certificates_list, 20)

    try:
        certificates = paginator.page(page)
    except PageNotAnInteger:
        certificates = paginator.page(1)
    except EmptyPage:
        certificates = paginator.page(paginator.num_pages)
    return render(request, 'certificates.html',
                  {'certificates': certificates, 'certificates_filter': certificate_filter})


@login_required
def get_certificates_for_admin(request):
    if request.method == "POST":
        # Fetch list of items to delete, by ID
        items_to_delete = request.POST.getlist('delete_items')
        # Delete those items all in one go
        Certificate.objects.filter(pk__in=items_to_delete).delete()

    certificates_list = Certificate.objects.all().order_by('user__username')
    certificate_filter = CertificateFilter(request.GET, queryset=certificates_list)
    certificates_list = certificate_filter.qs
    page = request.GET.get('page', 1)
    paginator = Paginator(certificates_list, 20)

    try:
        certificates = paginator.page(page)
    except PageNotAnInteger:
        certificates = paginator.page(1)
    except EmptyPage:
        certificates = paginator.page(paginator.num_pages)

    return render(request, 'admin_view.html', {'certificates': certificates, 'certificates_filter': certificate_filter})


def contact(request):
    return render(request, 'contact.html')


def terms_and_conditions(request):
    return render(request, 'terms.html')


def policy(request):
    return render(request, 'policy.html')


class ActivateAccountView(View):

    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.profile.email_confirmed = True
            user.save()
            login(request, user)
            return redirect('home')
        else:
            return render(request, 'account_activation_invalid.html')


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + datetime.timedelta(seconds=random_second)
