import datetime
from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    email_confirmed = models.BooleanField(default=False)

    def __unicode__(self):
        return self.user


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class Certificate(models.Model):
    RSA_1024 = '1024'
    RSA_2048 = '2048'
    RSA_4096 = '4096'

    SHA1 = 'sha1'
    SHA256 = 'sha256'

    KEY_CHOICE = (
        (RSA_1024, '1024'),
        (RSA_2048, '2048'),
        (RSA_4096, '4096')
    )

    SIGNATURE_CHOICE = (
        (SHA1, 'sha1'),
        (SHA256, 'sha256')
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    country = models.CharField(max_length=2)
    city = models.CharField(max_length=40)
    name = models.CharField(max_length=40)
    valid_from = models.DateField(help_text='Format is: yyyy-mm-dd')
    valid_to = models.DateField(help_text='Format is: yyyy-mm-dd')
    key = models.CharField(max_length=40, choices=KEY_CHOICE, default=RSA_1024)
    signature = models.CharField(max_length=40, choices=SIGNATURE_CHOICE, default=SHA256)
    generated_key = models.CharField(max_length=4096)

    def clean(self):

        # Don't allow dates older than now.
        if str(self.valid_from) < datetime.date.today().strftime('%Y-%m-%d'):
            raise ValidationError('valid_from must be later than now.')

        if self.valid_to < self.valid_from:
            raise ValidationError('valid_to must be later than valid_from.')

    def __unicode__(self):
        return self.name
