from app.models import Certificate
import django_filters


class CertificateFilter(django_filters.FilterSet):

    class Meta:
        model = Certificate
        fields = ['user__username', 'name', 'valid_from', 'valid_to']