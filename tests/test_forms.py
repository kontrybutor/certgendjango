import time
from django.test import LiveServerTestCase
from django.contrib.auth.models import User
from django.test import Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class GenerateFormTestCase(LiveServerTestCase):

    def setUp(self):
        self.selenium = webdriver.Safari()
        super(GenerateFormTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(GenerateFormTestCase, self).tearDown()

    def test_signup(self):

        selenium = self.selenium
        # Opening the link we want to test

        selenium.get('http://127.0.0.1:8000/signup')
        # find the form element

        username = selenium.find_element_by_id('id_username')
        email = selenium.find_element_by_id('id_email')
        password1 = selenium.find_element_by_id('id_password1')
        password2 = selenium.find_element_by_id('id_password2')
        submit = selenium.find_element_by_id('id_signup')

        # Fill the form with data
        username.send_keys('Test7')
        email.send_keys('temp@localhost.com')
        password1.send_keys('alamakota')
        password2.send_keys('alamakota')

        # submitting the form
        submit.click()
        time.sleep(10)

        # check the returned result
        assert 'Please confirm your email address to complete the registration.' in selenium.page_source
