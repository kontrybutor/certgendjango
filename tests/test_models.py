import datetime
from django.test import TestCase
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from app.models import Certificate


class GenerateTest(TestCase):

    def create_certficate(self, username='Amazon', name='Router', country='PL', city='Lodz', key='RSA_1024',
                          signature='SHA1',
                          valid_from=datetime.date(2018, 12, 28).strftime('%Y-%m-%d'),
                          valid_to=datetime.date(2018, 12, 29).strftime('%Y-%m-%d')):
        user = User.objects.create(username=username)
        return Certificate.objects.create(user=user, name=name, country=country, city=city, valid_from=valid_from,
                                          valid_to=valid_to, key=key,
                                          signature=signature)

    def test_certificate_creation(self):
        w = self.create_certficate()
        self.assertTrue(isinstance(w, Certificate))
        self.assertEqual(w.__unicode__(), w.name)

    def test_certificate_creation_with_invalid_time(self):
        w = self.create_certficate(valid_to=datetime.date(2018, 12, 29).strftime('%Y-%m-%d'),
                                   valid_from=datetime.date(2018, 12, 30).strftime('%Y-%m-%d'))
        self.assertTrue(isinstance(w, Certificate))
        self.assertRaises(ValidationError, lambda: w.clean())

    def create_profile(self, user='admin'):
        pass
