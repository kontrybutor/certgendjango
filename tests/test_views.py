import datetime
from django.urls import reverse
from django.test import TestCase
from django.contrib.auth.models import User
from app.models import Certificate



class ViewsTest(TestCase):


    def setUp(self):
        self.user = User.objects.create_user(username='Amazon', password='alamakota')

    def create_certficate(self, name='Router', country='PL', city='Lodz', key='RSA_1024',
                          signature='SHA1',
                          valid_from=datetime.date(2018, 12, 28).strftime('%Y-%m-%d'),
                          valid_to=datetime.date(2018, 12, 29).strftime('%Y-%m-%d')):

        return Certificate.objects.create(user=self.user, name=name, country=country, city=city, valid_from=valid_from,
                                          valid_to=valid_to, key=key,
                                          signature=signature)

    def test_contact_view(self):
        url = reverse("contact")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_terms_view(self):
        url = reverse("terms")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_policy_view(self):
        url = reverse("policy")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_home_view(self):
        self.client.login(username='Amazon', password='alamakota')
        url = reverse("home")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
